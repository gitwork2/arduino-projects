# Arduino Radar
## Arduino Radar - это проект позволяющий обнаруживать обЪекты благодаря ультразвуковым волнам.

Проект собран на базе Arduino Nano и сонара HY-SRF05. Он определять растояние до предмета даже в темноте и выводит все данные графически на компьютер.
Он может помочь в сканировании местности темной точью или автоматическом определении обЪектов, спектр его задачь может свободно расширяться.

![Soanr](https://github.com/koshcheev713/github_git/blob/main/photos/IMG20240514174239.jpg)
![Sonny and Mariel high fiving.](https://content.codecademy.com/courses/learn-cpp/community-challenge/highfive.gif)
